import React, { useState, useEffect } from 'react';
import Button from './components/atoms/Button';
import Title from './components/atoms/Title'
import Icon from './components/atoms/Icon';
import FooterItem from './components/molecules/FooterItem';
import './App.scss';

const App = () => {
 
  return (
  <div className="app-wrapper">
    <div className="app-wrapper__header">
    <div className="app-wrapper__header-cont app-wrapper__header-cont--left-img">
      <img src={require('./assets/image/yellow.png')} alt='Cuida tu información bancaria, Landing Bci ' />
    </div>
    <div className="app-wrapper__header-cont app-wrapper__header-cont--center-img">
      <img src={require('./assets/image/logo.png')} alt='El banco esta en tus manos cuida tu información bancaria, Landing Bci ' />
    </div>
    <div className="app-wrapper__header-cont app-wrapper__header-cont--right-img">
      <img src={require('./assets/image/green.png')} alt='Cuida tu información bancaria, Landing Bci ' />
    </div>
    
    </div>
    
     <div className="app-wrapper__main">
     <div className="app-wrapper__main-bottons">
      <div className="app-wrapper__main-bottons-item">
        <Button typeButton="primaryRed" textButton="¿Qué puedo hacer?" />
      </div>
      <div className="app-wrapper__main-bottons-item">
       <Button typeButton="primaryBlue" textButton="Recomendaciones Vacaciones"/>  
      </div>
     <div className="app-wrapper__main-bottons-item">  
       <Button typeButton="primaryGreen" textButton="Beneficios"/>
     </div>
     
     </div>
     <div className="app-wrapper__main-left">
     <div className="app-wrapper__main-left-title">
     <span className="acent-color">Descarga la App BCI Móvil </span>y vive
     la experiencua de llevar tu banco a todas partes
     </div>
     <div className="icon-wrapper"><Icon textIcon="Saldos" typeIcon="saldos" /></div>
     <div className="icon-wrapper"><Icon textIcon="Transferencias" typeIcon="transferencias"/></div>
     <div className="icon-wrapper"><Icon textIcon="Pagos" typeIcon="pagos"/></div>
     <div className="icon-wrapper"><Icon textIcon="Creditos" typeIcon="creditos"/></div>
     

     <div className="icon-wrapper"><Icon textIcon="Tarjetas" typeIcon="tarjetas" /></div>
     <div className="icon-wrapper"><Icon textIcon="Seguros" typeIcon="seguros"/></div>
     <div className="icon-wrapper"><Icon textIcon="Cajeros y Sucursales" typeIcon="cajeros"/></div>
     <div className="icon-wrapper"><Icon textIcon="Más Servicios" typeIcon="servicios"/></div>
      
     </div>
     <div className="app-wrapper__main-right">
      <img src={require('./assets/image/bciApp.png')} alt='Cuida tu información bancaria, Landing Bci ' />
    
     </div>
    </div>

  <div className="app-wrapper__footer">
      <h3>¡Estas vacaciones cuídate del Skimming o Clonación de Tarjetas!</h3>
      <div className="app-wrapper__footer-item ">
      <FooterItem testId="item1-footer" numberItem="uno"/>
      </div> 
      <div className="app-wrapper__footer-item ">
      <FooterItem testId="item2-footer" numberItem="dos"/>
      </div> 
      <div className="app-wrapper__footer-item ">
      <FooterItem testId="item3-footer"numberItem="tres" />
      </div> 
      <div className="app-wrapper__footer-item ">
      <FooterItem testId="item4-footer"numberItem="cuatro" />
      </div> 
      
    </div>
  </div>
 )}

export default App;
