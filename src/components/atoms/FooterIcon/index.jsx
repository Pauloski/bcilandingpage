import React from 'react';
import PropTypes from 'prop-types';
import style from './footerIcon.module.scss'
export const typeIcon = ['uno', 'dos', 'tres', 'cuatro'];


const FooterIcon = ({ testId, typeIcon }) => (
  <div data-testid={testId} className={ `${style[`f-icon--${typeIcon}`]} ${style[`f-icon`]}` } />
);
FooterIcon.propTypes = {
  typeIcon: PropTypes.oneOf(['uno', 'dos', 'tres', 'cuatro']),
  testId: PropTypes.string,
}

FooterIcon.defaultProps = {
  typeIcon: 'uno',
  testId: 'defaultIcon',

}
export default FooterIcon; 