import React from 'react';
import PropTypes from 'prop-types';
import style from './button.module.scss'
export const typeButton = ['primaryBlue', 'primaryRed', 'primaryGreen'];
//export default { title: "MDX/Button" component: Button };
const Button = ({ testId, textButton,typeButton, actionButton }) => (
  <button data-testid={testId} className={ `${style[`button--${typeButton}`]} ${style[`button`]}` } onClick={actionButton}>
   {textButton}
  </button>
);
Button.propTypes = {
  typeButton: PropTypes.oneOf(['primaryBlue', 'primaryRed','primaryGreen']),
  textButton: PropTypes.string,
  testId: PropTypes.string,
  actionButton: PropTypes.any
}

Button.defaultProps = {
  typeButton: 'primaryBlue',
  textButton: 'Soy un Botón',
  testId: 'botonPrimario',

}
export default Button; 