import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { withKnobs, text,  select } from "@storybook/addon-knobs";
//import { Button } from './index';
import Button, { testId, textButton, typeButton, actionButton } from './index';
export default {
  title: "Storybook Knobs",
  decorators: [withKnobs]
};
storiesOf('Atoms.Button', module)
.addDecorator(withKnobs)
.add('Button', () => 
<>
<Button 
testId={ text('id', 'default')} 
textButton={ text('Texto de botón', 'Componente de botón')} 
typeButton={select('tipo de botón',typeButton, typeButton[0])} 
actionButton={action('clicked')}
/>
</>
)