import React from 'react';
import PropTypes from 'prop-types';
import style from './icon.module.scss'
export const typeIcon = ['pagos', 'saldos', 'seguros', 'servicios', 'tarjetas', 'transferencias', 'cajeros', 'creditos'];


const Icon = ({ testId, textIcon,typeIcon }) => (
  <div data-testid={testId} className={ `${style[`icon--${typeIcon}`]} ${style[`icon`]}` } >
    <span className={`${style[`icon__text`]}`}>{textIcon}</span>
  </div>

);
Icon.propTypes = {
  typeIcon: PropTypes.oneOf(['pagos', 'saldos', 'seguros', 'servicios', 'tarjetas', 'transferencias', 'cajeros', 'creditos']),
  textIcon: PropTypes.string,
  testId: PropTypes.string,
}

Icon.defaultProps = {
  typeIcon: 'pagos',
  textIcon: 'Soy un icono',
  testId: 'defaultIcon',

}
export default Icon; 