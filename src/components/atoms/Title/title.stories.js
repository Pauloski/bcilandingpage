import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text,  select } from "@storybook/addon-knobs";
import Title, { FONTSIZE, FAMILY, WEIGHT, ALIGN, SEOTYPE }  from './index';

export default {
  title: "Storybook Knobs",
  decorators: [withKnobs]
};


storiesOf('Atoms.Title', module)
.addDecorator(withKnobs)
.add('Title', () => <Title 
  testId={ text('id', 'default')} 
  textComponent={ text('Texto', 'Componente de título')} 
  fontSize={select('FontSize',FONTSIZE, FONTSIZE[0])} 
  family={select('Family',FAMILY, FAMILY[0])}  
  weight={select('Weight',WEIGHT, WEIGHT[0])} 
  align={select('Align',ALIGN, ALIGN[0])} 
  seoType={select('SeoType',SEOTYPE, SEOTYPE[0])}
  />
  )
