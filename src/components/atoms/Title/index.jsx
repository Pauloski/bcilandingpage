import React from 'react';
import Proptypes from 'prop-types';
import Style from './title.module.scss';
export const FONTSIZE = [
  'small', 
  'normal', 
  'caption', 
  'medium', 
  'large', 
  'heading3', 
  'heading2', 
  'heading1'
];
export const FAMILY = ['OpenSans', 'Sans-serif', 'Anton', 'Monserrat'];
export const ALIGN = ['center', 'left', 'right'];
export const WEIGHT= ['bold', 'normal'];
export const SEOTYPE= ['h1', 'h3', 'h4', 'div'];

const Title = (
  {family, textComponent, weight, align, fontSize, seoType, testId}) => {
  
  if(seoType==="h1") {
    return ( 
      <h1
       data-testid ={`title--${testId}`}
         className={`${Style[`title--${weight}`]}
       ${Style[`title--${align}`]}
       ${Style[`title--${family}`]}
       ${Style[`title--font-${fontSize}`]}`}
     >
       { textComponent }
     </h1>
     )
   }

   if(seoType==="h3") {
    return ( 
      <h3
       data-testid ={`title--${testId}`}
         className={`${Style[`title--${weight}`]}
       ${Style[`title--${align}`]}
       ${Style[`title--${family}`]}
       ${Style[`title--font-${fontSize}`]}`}
     >
       { textComponent }
     </h3>
     )
   }

   if(seoType==="h4") {
    return ( 
      <h4
       data-testid ={`title--${testId}`}
         className={`${Style[`title--${weight}`]}
       ${Style[`title--${align}`]}
       ${Style[`title--${family}`]}
       ${Style[`title--font-${fontSize}`]}`}
     >
        { textComponent }
     </h4>
     )
   } 
   
   if(seoType==="div") {
    return ( 
      <div
       data-testid ={`title--${testId}`}
         className={`${Style[`title--${weight}`]}
       ${Style[`title--${align}`]}
       ${Style[`title--${family}`]}
       ${Style[`title--font-${fontSize}`]}`}
     >
        { textComponent }
     </div>
     )
   } 
};

Title.propTypes = {
  testId: Proptypes.string,
  ttextComponent: Proptypes.string,
  family: Proptypes.oneOf(['OpenSans', 'Sans-serif', 'Anton', 'Monserrat']),
  color: Proptypes.oneOf(['dark', 'light']),
  fontSize: Proptypes.oneOf([ 'small', 
  'normal', 
  'caption', 
  'medium', 
  'large', 
  'heading3', 
  'heading2', 
  'heading1']),
  weight: Proptypes.oneOf(['bold', 'normal']),
  align: Proptypes.oneOf(['center', 'left', 'right']),
  seoType: Proptypes.oneOf(['h1', 'h3', 'h4', 'div']),
};

Title.defaultProps = {
  testId: 'default',
  family: 'OpenSans',
  textComponent: 'Componente de título',
  fontSize: 'caption',
  weight: 'normal',
  align: 'left',
  seoType: 'div',
};

export default Title;