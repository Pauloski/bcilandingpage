/* eslint-disable default-case */
import React from 'react';
import PropTypes from 'prop-types';
import FooterIcon from '../../atoms/FooterIcon';
import style from './footerItem.module.scss'


const project = (param, id) => {
  switch (param) {
    case "uno":
      return (
        <div data-testid= {id} className={`${style[`item`]} `} >
          <div className={`${style[`item__cont-icon`]} `}>
            <FooterIcon typeIcon="uno" data-testid= {`${id[`-footerIcon`]} `}/>
          </div>
          <div className={`${style[`item__cont-info`]} `}>
            <div className={`${style[`item__title`]} `}>Concejo 1:</div>
            <div className={`${style[`item__paragraph`]} `}>
              Realiza directamente tus transacciones o mantén tu tarjeta a la vista todo el tiempo.
     </div>
          </div>
        </div>
      );
    case "dos":
      return (

        <div data-testid= {id} className={`${style[`item`]} `} >
          <div className={`${style[`item__cont-icon`]} `}>
          <FooterIcon typeIcon="dos"  data-testid= {`${id[`-footerIcon`]} `}/>
          </div>
          <div className={`${style[`item__cont-info`]} `}>
            <div className={`${style[`item__title`]} `}>Concejo 2:</div>
            <div className={`${style[`item__paragraph`]} `}>
              Cuando uses cajeros automáticos cubre con tu mano la contraseña</div>
          </div>
        </div>
      );
    case "tres":
      return (

        <div data-testid= {id} className={`${style[`item`]} `} >
          <div className={`${style[`item__cont-icon`]} `}>
            <FooterIcon typeIcon="tres"  data-testid= {`${id[`-footerIcon`]} `}/>
          </div>
          <div className={`${style[`item__cont-info`]} `}>
            <div className={`${style[`item__title`]} `}>Concejo 3:</div>
            <div className={`${style[`item__paragraph`]} `}>
              Nunca permitas que fotografíen tus tarjetas.</div>
          </div>
        </div>
      );
    case "cuatro":
      return (

        <div data-testid= {id} className={`${style[`item`]} `} >
          <div className={`${style[`item__cont-icon`]} `}>
            <FooterIcon typeIcon="cuatro"  data-testid= {`${id[`-footerIcon`]} `}/>  
          </div>
          <div className={`${style[`item__cont-info`]} `}>
            <div className={`${style[`item__title`]} `}>Concejo 4:</div>
            <div className={`${style[`item__paragraph`]} `}>
              No des nunca tu clave en voz alta.</div>
          </div>
        </div>
      );
    case "null":
      return (

        <div  className={`${style[`item`]} `} >
          <div className={`${style[`item__cont-paragraph`]} `}>Cuida tu información bancaria.</div>
        </div>
      );
    case "default":
      return (

        <div className={`${style[`item`]} `} >
          <div className={`${style[`item__cont-paragraph`]} `}>Cuida tu información bancaria.</div>
        </div>
      );

  }
}

const FooterItem = ({ testId, numberItem }) => (  project(numberItem, testId) )
  
FooterItem.propTypes = {
  numberItem: PropTypes.oneOf(['uno', 'dos', 'tres', 'cuatro']),
  testId: PropTypes.string,
}

FooterItem.defaultProps = {
  numberItem: 'uno',
  testId: 'defaultItem',

}
export default FooterItem; 